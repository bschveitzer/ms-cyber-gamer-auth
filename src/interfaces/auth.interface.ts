export interface IAuthReq {
  email: string,
  password: String,
}

export interface IAuthModel {
  email: String,
  password: String,
  permission: String
}

export interface ICreateReq {
  name: String,
  email: String,
  password: String,
  phone: String,
  gender: String,
  birthdate: Date
}