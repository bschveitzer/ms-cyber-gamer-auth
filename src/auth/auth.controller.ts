import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { MessagePattern } from '@nestjs/microservices';
import { IAuthReq, ICreateReq } from '../interfaces/auth.interface'
import { JwtAuthGuard } from './jwt-auth.guard';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @MessagePattern('doAuth')
  async login(data: IAuthReq): Promise<any> {
    const validations = await this.authService.validateUser(data);
    
    if(validations) {
      return await this.authService.doLogin(data);
    }
  }

  @UseGuards(JwtAuthGuard)
  @MessagePattern('createUser')
  async createUser(data: ICreateReq): Promise<any> {
    return await this.authService.createUser(data);
  }

  @UseGuards(JwtAuthGuard)
  @MessagePattern('deleteUser')
  async deleteUser(data: string): Promise<any> {
    return await this.authService.deleteUser(data);
  }

  @UseGuards(JwtAuthGuard)
  @MessagePattern('deleteUser')
  async updateUser(data: ICreateReq): Promise<any> {
    return await this.authService.updateUser(data);
  }
}
