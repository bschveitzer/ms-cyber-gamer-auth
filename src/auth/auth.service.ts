import { Injectable } from '@nestjs/common';
import { ClientProxyFactory, Transport, ClientProxy } from '@nestjs/microservices';
import { Observable } from 'rxjs';
import { InjectModel } from '@nestjs/mongoose';
import { Auth } from 'src/models/auth.model';
import { Model } from 'mongoose';
import { IAuthReq, IAuthModel, ICreateReq } from 'src/interfaces/auth.interface';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  private msUserClient: ClientProxy;

  constructor(
    @InjectModel(Auth.name) private authModel: Model<Auth>, private jwtService: JwtService) {
    this.msUserClient = ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        host: '127.0.0.1',
        port: 8878
      }
    })
  }

  async getUser(data: string): Promise<any> {
    return this.msUserClient.send<string, string>('getUser', data);
  }

  async validateUser(data: IAuthReq): Promise<boolean> {
    const user: Auth = await this.authModel.findOne({ email: data.email })
    if(user && user.password === data.password) return true;
    return false;
  }

  async doLogin(data: IAuthReq) {
    return {
      access_token: this.jwtService.sign(data),
    };
  }

  async createUser(data: ICreateReq) {
    return this.msUserClient.send<string, ICreateReq>('createUser', data);
  }

  async deleteUser(data: string) {
    return this.msUserClient.send<string, string>('deleteUser', data);
  }

  async updateUser(data: ICreateReq) {
    return this.msUserClient.send<string, ICreateReq>('createUser', data);
  }
}
