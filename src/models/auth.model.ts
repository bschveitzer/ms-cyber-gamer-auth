import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ timestamps: true })
export class Auth extends Document {
  @Prop({ required: true, lowercase: true, unique: true})
  email: string;

  @Prop({ required: true, lowercase: true })
  password: string;

  @Prop({ required: true })
  permission: string;
}

export const AuthModel = SchemaFactory.createForClass(Auth);